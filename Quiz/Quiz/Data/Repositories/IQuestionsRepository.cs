using System.Collections.Generic;
using System.Threading.Tasks;
using Quiz.Models;

namespace nearHardware.Data
{
    public interface IQuestionsRepository
    {
        Task<IEnumerable<Questions>> GetAllQuestionsAsync();

        Task<Questions> GetQuestionAsync(int question_id);

        Task InsertQuestionAsync(bool identityMode, Questions question);

        Task UpdateQuestionAsync(Questions question);
        Task DeleteQuestionAsync(int question_id);
    }
}