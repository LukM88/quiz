using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace nearHardware.Data
{
    public abstract class GenericRepository<T> :
        IGenericRepository<T> where T : class
    {
        private readonly IConfiguration configuration;
        private readonly string tableName;


        /// <summary>
        /// Create generic repository instance
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <param name="configuration">Configuration object contain database connection string</param>
        public GenericRepository(string tableName, IConfiguration configuration)
        {
            // Fill all properties
            this.tableName = tableName;
            this.configuration = configuration;
        }

        /// <summary>
        /// Get all records from table in asynchronous mode
        /// </summary>
        /// <returns>All records from table</returns>
        public async Task<IEnumerable<T>> GetAllAsync()
            => await CreateConnection().QueryAsync<T>($"SELECT * FROM {tableName};");


        /// <summary>
        /// Get single record from table in asynchronous mode 
        /// </summary>
        /// <param name="t_id"></param>
        /// <returns>Single record from table</returns>
        /// <exception cref="KeyNotFoundException">it is report when any record has not found</exception>
        public async Task<T> GetAsync(int t_id)
        {
            // Open new connection
            using (var connection = CreateConnection())
            {
                // Execute get query
                var getQueryResult = await connection.QuerySingleOrDefaultAsync<T>(
                    $"SELECT * FROM {tableName} WHERE Id=@Id", new { Id = t_id });

                // Validate get query response
                if (getQueryResult == null)
                {
                    throw new KeyNotFoundException(
                        $"{tableName} with id [{t_id}] could not be found.");
                }

                // Return get query result
                return getQueryResult;
            }
        }

        /// <summary>
        /// Insert single record to table in asynchronous mode
        /// </summary>
        /// <param name="t">Record's model</param>
        /// <returns>Task flag</returns>
        public async Task InsertAsync(bool identityMode, T t)
            => await CreateConnection().ExecuteAsync(GenerateInsertQuery(identityMode: identityMode), t);

        /// <summary>
        /// Update single record in table in asynchronous mode
        /// </summary>
        /// <param name="t">Record's model</param>
        /// <returns>Task flag</returns>
        public async Task UpdateAsync(T t)
            => await CreateConnection().ExecuteAsync(GenerateUpdateQuery(), t);

        /// <summary>
        /// Delete single record from table in asynchronous mode
        /// </summary>
        /// <param name="t_id"></param>
        /// <returns>Task flag</returns>
        public async Task DeleteAsync(int t_id)
            => await CreateConnection().ExecuteAsync($"DELETE FROM {tableName} WHERE Id=@Id", new { Id = t_id });

        /// <summary>
        /// Generate new connection based on connection string
        /// </summary>
        /// <returns>Object represent new connection</returns>
        private SqlConnection SqlConnection()
            => new SqlConnection(configuration.GetConnectionString("DefaultConnection"));

        /// <summary>
        /// Open new connection & return it for use
        /// </summary>
        /// <returns>Object represent opened connection</returns>
        private IDbConnection CreateConnection()
        {
            // Generate new connection
            var connection = SqlConnection();

            // Open & return new connection information
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Rating T class & generate it properties container
        /// </summary>
        /// <returns>T class's properties container</returns>
        private IEnumerable<PropertyInfo> GetProperties()
            => typeof(T).GetProperties();

        /// <summary>
        /// Validate T class's properties container & conversion it to string container
        /// </summary>
        /// <param name="listOfProperties">T class's properties container</param>
        /// <returns>T class's properties in string container form</returns>
        private static List<string> GenerateListOfProperties(
            IEnumerable<PropertyInfo> listOfProperties)
            => listOfProperties.Select<PropertyInfo, string>((singleProperty) =>
            {
                // Get single property's metadata container
                var metaData = singleProperty.GetCustomAttributes(typeof(DescriptionAttribute), false);

                // Validate single property's
                if ((metaData.Length <= 0) ||
                    ((metaData[0] as DescriptionAttribute)?.Description != "ignore"))
                {
                    return singleProperty.Name;
                }
                else
                {
                    return null;
                }
            }).ToList();


        /// <summary>
        /// Generate insert query string
        /// </summary>
        /// <returns>Insert query string</returns>
        private string GenerateInsertQuery(bool identityMode)
        {
            // Init insert query string
            var insertQuery = new StringBuilder($"INSERT INTO {tableName} (");


            // Generate insert query properties
            var insertQueryProperties = GenerateListOfProperties(GetProperties());

            // Adaptation insert query form to table's identity mode
            if (identityMode == true)
            {
                insertQueryProperties.RemoveAt(0);
            }

            // Fill insert query the properties
            insertQueryProperties.ForEach((singleProperty) => { insertQuery.Append($"{singleProperty},"); });

            // Prepare insert query to fill it the values in dapper notation
            insertQuery.Remove((insertQuery.Length - 1), 1).Append(") VALUES (");

            // Fill insert query the values in dapper notation
            insertQueryProperties.ForEach((singleProperty) => { insertQuery.Append($"@{singleProperty},"); });

            // Prepare insert query to share it
            insertQuery.Remove(insertQuery.Length - 1, 1).Append(")");

            // Return final insert query form
            return insertQuery.ToString();
        }

        /// <summary>
        /// Generate update query string
        /// </summary>
        /// <returns>Update query string</returns>
        private string GenerateUpdateQuery()
        {
            // Init update query string
            var updateQuery = new StringBuilder($"UPDATE {tableName} SET ");


            // Generate update query properties
            var updateQueryProperties = GenerateListOfProperties(GetProperties());

            // Prepare update query to fill it the properties and values
            updateQueryProperties.Remove(updateQueryProperties.First());

            // Fill update query the properties and values
            updateQueryProperties.ForEach((updateQueryProperty) =>
            {
                updateQuery.Append($"{updateQueryProperty}=@{updateQueryProperty},");
            });

            // Fill update query's condition
            updateQuery.Remove(updateQuery.Length - 1, 1).Append(" WHERE Id=@Id");

            // Return final update query form
            return updateQuery.ToString();
        }
    }
}