using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Quiz.Models;

namespace nearHardware.Data
{
    public class QuestionsRepository : GenericRepository<Questions>, IQuestionsRepository
    {
        public QuestionsRepository(IConfiguration configuration)
            : base(tableName: "questions", configuration: configuration)
        {
        }

        public async Task<IEnumerable<Questions>> GetAllQuestionsAsync()
            => await GetAllAsync();

        public async Task<Questions> GetQuestionAsync(int question_id)
            => await GetAsync(question_id);

        public async Task InsertQuestionAsync(bool identityMode, Questions question)
            => await InsertAsync(identityMode: identityMode, t: question);

        public async Task UpdateQuestionAsync(Questions question)
            => await UpdateAsync(t: question);

        public async Task DeleteQuestionAsync(int question_id)
            => await DeleteAsync(t_id: question_id);
    }
}