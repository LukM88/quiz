using System.Collections.Generic;
using System.Threading.Tasks;

namespace nearHardware.Data
{
    public interface IGenericRepository<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetAsync(int t_id);
        Task InsertAsync(bool identityMode, T t);
        Task UpdateAsync(T t);
        Task DeleteAsync(int t_id);
    }
}