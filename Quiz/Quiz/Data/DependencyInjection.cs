using Microsoft.Extensions.DependencyInjection;
using nearHardware.Data;

namespace Quiz.Models
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
            => services.AddTransient<IQuestionsRepository, QuestionsRepository>();
    }
}