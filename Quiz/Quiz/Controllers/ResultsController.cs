﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quiz.Models;
using Quiz.ViewModels;

namespace Quiz.Controllers
{
    public class ResultsController : Controller
    {
        [HttpGet]
        public IActionResult Index(ResultsVM resultsVM)
        {

            return View(resultsVM);
        }
    }
}
