﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quiz.Models;
using nearHardware.Data;
using Quiz.ViewModels;

namespace Quiz.Controllers
{
    public class QuizController : Controller
    {

        private readonly IQuestionsRepository questionsRepository;
        Dictionary<int, Questions> tenQuestions;
        public QuizController(IQuestionsRepository questionsRepository)
            => this.questionsRepository = questionsRepository;
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            tenQuestions = new Dictionary<int, Questions>();
            var dbquestions = await questionsRepository.GetAllQuestionsAsync();
            List<Questions> questions = dbquestions.ToList(); 


            for (int i = 0; i < 10; i++)
            {
                Questions question = questions.ToList()[new Random().Next(questions.ToList().Count)];
                tenQuestions.Add(question.Id, question);
                questions.Remove(question);
            }
            return View(new QuizVM { questions = tenQuestions });

        }
        [HttpPost]
        public async Task<IActionResult> Index(QuizVM quizVM)
        {
            var dbquestions = await questionsRepository.GetAllQuestionsAsync();
            int score = 0;
            foreach(Questions question in dbquestions.ToList())
            {
                if (quizVM.questions.Keys.ToList().Contains(question.Id))
                {
                    if (quizVM.questions[question.Id].UserAnswer == question.Answer)
                    {
                        score += 1;
                    }
                }
            }
            return RedirectToAction("Index", "Results", new ResultsVM { score = score });
        }
    }
}
