﻿using Microsoft.AspNetCore.Mvc;
using Quiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz.ViewModels
{
    public class QuizVM
    {
        public Dictionary<int, Questions> questions { get; set; }
    }
}
