#!bin/bash
sudo docker stop jwilder
sudo docker rm jwilder
sudo docker stop quiz
sudo docker rm quiz
sudo docker image rm quzi
version="wersja1"
if [ -d quiz ]; then
    echo "Deleting catalog quiz"
    rm -r -f quiz 
fi
echo "Cloning repository"
git clone https://gitlab.com/LukM88/quiz.git &&\
cd quiz &&\
git checkout tags/$version
echo "Pulling and building images"
sudo docker pull jwilder/nginx-proxy
sudo docker build Quiz/ -t "quiz" &&\
echo "Launching containers"
sudo docker run -d --name jwilder -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
sudo docker run -d --name quiz -e VIRTUAL_HOST=quiz.pl quiz
