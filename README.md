# Quiz

## Opis

Aplikacja Quiz jest to aplikacja w technologi .Net Core 3.1 MVC. Aplikacja jest stroną internetową realizowaną w ramach projektu na studia z zajęć o technologiach chmurowych. Wykorzystuje ona bazę danych przechowywaną w chmurze Microsofr Azure, a platformą wdrożeniową jest Docker 20.10.8 postawiony na systemie Ubuntu 20.04.

## CI

Repozytorium posiada zaimplementowany moduł gitlab CI w którym przy stworzeniu merge requesta włączany jest proces budowania aplikacji z wdrażanymi zmianami w celu uniknięcia błędów na głównej gałęzi projektu.

## Wdrażanie

Aby zbudować i wdrożyć aplikację konieczny jest zainsalowany dokcer engin i docker cli. Więcej o instalacji dockera pod [linkiem][1]. Następnie w katalogu `utils` znajduje sie skrypt [pull_and_build.sh][3] należy go pobrać i włączyć w folderze gdzie ma być zapisana aplikacja. Skrypt pobierze wtedy całe repozytorium, zbuduje, oraz włączy aplikacje pod adresem `quiz.pl` na localhoscie. Usunie on także starsze wersje aplikacji oraz kontener `jwilder/nginx-proxy`.

### Budowanie

W katalogu projektu znajduje się plik [Dockerfile][2] w którym jest zestaw instrukcji używanych do zbudowania obrazu aplikacji. Celem zbudowania obrazu należy użyć komendy `docker build Quiz` włączonej w katalogu z folderem Quiz. Aby sprawdzić czy obraz został utworzony należy użyć polecenia `docker image ls`

### Włączenie aplikacji

W momencie gdy mamy już zbudowany obraz aplikacji należy użyć polecenia `docker run -d --name jwilder -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy` aby stworzyć kontener odpowiedzialny za proxy, oraz `docker run -d --name quiz -e VIRTUAL_HOST=quiz.pl <nazwa/numer id obrazu>` aby stworzyć kontener z aplikacją. Zostanie wtedy utworzony kontener o nazwie quiz wystaawiony pod adresem `quiz.pl`. Domyślnie ip dockera to 172.17.0.1, a kolejne kontenery wystawione w sieci są wystawiane na kolejnych numerach ip. Celem sprawdzenia adresu ip kontenera używamy polecenia `docker network inspect bridge`. aby dostac się do aplikacji na za pomocą adresu należy dodać taki wpis do pliku `hosts` w systemie użytkownika.
```
0.0.0.0 quiz.pl
```
W przypadku innej maszyny w sieci lokalnej która ma łączyć się z aplikacją należy dodać ip serwera np. 
```
10.10.10.3 quiz.pl
```

### Sprawdzenie działania

Aby sprawdzić działający kontener należy w przeglądarce internetowaej wpisać adres ip contenera np. `172.17.0.2` lub w konsoli systemowej wpisać `docker ps` powinniśmy wtedy zobaczyć 2 kontenery jeden o nazwie quiz a drugi o nazwie jwilder.

### Wyłączanie i usuwanie

Wyłączanie kontenera odbywa się za pomocą polecenia `docker stop quiz`, a usunięcie kontenera `docker rm quiz`

[1]: https://docs.docker.com/engine/
[2]: ./Quiz/Dockerfile
[3]: ./utils/pull_and_build.sh